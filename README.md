# Express Multiplication API

This is a simple Express.js application that provides a REST API with two endpoints:

- A "Hello World" endpoint at the root (`/`)
- A "Multiply" endpoint (`/multiply`)

## Endpoints

### GET /

Responds with "Hello World! This is a GET request."

### GET /multiply

Takes two query parameters, `a` and `b`, and returns their product. If either `a` or `b` is not a valid number, it responds with "Couldn't multiply. Please check your query parameters."

Example: `http://127.0.0.1:3000/multiply?a=3&b=5`

## Prerequisites

- Node.js
- npm

## Installation

After cloning the repository, install the dependencies with:

```sh
npm i

Then, start the server:

node app.js

Or, if you are using Node.js version 20 or later, you can use the --watch flag to automatically restart the server when the source code changes:

node --watch app.js
```

The server will start listening at http://127.0.0.1:3000

