const express = require('express');
const app = express();
const port = 3000;

// GET endpoint - http://127.0.0.1:3000/
app.get('/', (req, res) => {
  res.send('Hello World! This is a GET request.');
});

/**
 * This function mulpilies multiplicant with multiplier
 * @param {number} multiplicant first parameter
 * @param {number} multiplier second parameter
 * @returns {number} product
 */
const multiply = (multiplicant, multiplier) => {
    const product = multiplicant * multiplier;
    return product;
};

// GET multiple endpoints - http://127.0.0.1:3000/multiply?a=3&b=5
app.get('/multiply', (req, res) => {
    try {
        const multiplicant = parseFloat(req.query.a);
        const multiplier = parseFloat(req.query.b);
        if (isNaN(multiplicant) || isNaN(multiplier)) {
            throw new Error("Invalid query parameters");
        }
        console.log({multiplicant, multiplier});
        const product = multiply(multiplicant, multiplier);
        res.send(product.toString(10));
    } catch (error) {
        console.error(error.message);
        res.send("Couldn't multiply. Please check your query parameters.");
    }
});


app.listen(port, () => console.log(
    `Example app listening at http://127.0.0.1: on port ${port}!`
));
